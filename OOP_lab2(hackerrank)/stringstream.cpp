#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    stringstream ss(str);
    vector <int> myvector;
    char ch;
    int a,repeat=0;
    for (long int i = 0; i < 800000;i++) {
        ss >> a >> ch;
        if (a == repeat) {
            break;
        }
        else
         myvector.push_back(a);
        repeat = a;
    }
    
     return myvector;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}