#include <string>
#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;
    string s[] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    
    if (n > 9) {
        cout << "Greater than 9" << endl;
    }
    else {
        cout << s[n - 1] << endl;
    }
    return 0;
}