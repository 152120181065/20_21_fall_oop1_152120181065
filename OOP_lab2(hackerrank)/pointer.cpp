#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void update(int* a, int* b) {
    int sum, min;
    sum = *a + *b;
    min = abs(*a-*b);
    *a = sum;
    *b = min;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    cin >> a >> b;
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}