#include <iostream>
#include <string>
using namespace std;

class Student {
private:
    int age;
    int standard;
    string first_name;
    string last_name;
public:
    void set_age(int age_out) {
        age = age_out;
    }
    void set_standard(int standard_out) {
        standard = standard_out;
    }
    void set_firstName(string firstName_out)
    {
        first_name = firstName_out;
    }
    void set_lastName(string lastName_out)
    {
        last_name = lastName_out;
    }
    int get_age() {
        int age_out = age;
        return age_out;
    }
    int get_standard() {
        int standard_out = standard;
        return standard_out;
    }
    string get_firstName()
    {
        string firstname_out = first_name;
        return firstname_out;
    }
    string get_lastName()
    {
        string lastname_out = last_name;
        return lastname_out;
    }
    void to_string() {
       
        cout << age <<',' << first_name << ',' << last_name << ',' << standard;
    }
       
};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_firstName(first_name);
    st.set_lastName(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_lastName() << ", " << st.get_firstName() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    st.to_string();

    return 0;
}