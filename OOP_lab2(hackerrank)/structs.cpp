#include <iostream>
#include <string>

using namespace std;

struct Student {
	int Age;
	string firstName;
	string lastName;
	int standart;
};

int main() {
    Student st;

    cin >> st.Age >> st.firstName >> st.lastName >> st.standart;
    cout << st.Age << " " << st.firstName << " " << st.lastName << " " << st.standart;

    return 0;
}