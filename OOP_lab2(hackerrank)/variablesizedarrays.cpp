#include <vector>
#include <iostream>
using namespace std;


int main() {
    int input1, input2, size, value, row, col;
    cin >> input1 >> input2;
    vector<vector<int>> nvec;
    for (int i = 0; i < input1; ++i) {
        cin >> size;
        vector<int> ivec;
        for (int j = 0; j < size; ++j) {
            cin >> value;
            ivec.push_back(value);
        }
        nvec.push_back(ivec);
    }
    for (int k = 0; k < input2; ++k) {
        cin >> row >> col;
        cout << nvec[row][col] << endl;
    }
    return 0;
}