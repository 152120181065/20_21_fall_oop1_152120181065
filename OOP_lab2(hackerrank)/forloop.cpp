#include <iostream>
#include <string>
using namespace std;

int main() {
    int n, m, num;
    cin >> n;
    cin >> m;
    string arr[9] = { "one","two","three","four","five","six","seven","eight","nine" };

    for (int k = n; k <= m; k++) {
        if (k > 0 && k <= 9) {
            cout << arr[k - 1] << endl;
        }
        else {
            if (k % 2 == 1)
                cout << "odd" << endl;
            else if (k % 2 == 0)
                cout << "even" << endl;
        }

    }


    return 0;
}