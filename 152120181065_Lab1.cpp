#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

int sum(int *, int);
int pro(int *, int);
int avg(int *, int);
int smallest(int *, int);

void main(void) {

	///Our main funciton to solve prombles.
	int inputnumber;/** @param inputnumber is for keeping numbers of how many inputs there.
					* @param summation keep result of summation of numbers.
					* @param product keep result of production of numbers.
					* @param average keep result of average calculation of numbers.
					*/
	
	int summation,product,average,smallestr;
	string fileName; /** @param fileName is for opening your document with its name.*/
	///Firstly taking input for document's name.
	cout << "Please enter your file name: ";
	cin >> fileName;
	///Then opening our file with its name by using ifstream library.
	ifstream dataFile(fileName);
	///Checking if our file is open.
	if (dataFile.is_open())
	{
		///Printing a message for user if file is open.
		cout << "The file " << fileName << " was opened successfully.\n";
	}
	else
	{
		///Printing a message for user if file could not open.
		cout << "The file " << fileName << " was not opened.\n";
	}
	system("pause");
	system("CLS");//This is for deleting our message from screen. 
	/**
	It is starting our problem solving.
	Firstly it takes numbers from .txt file.
	Then it is keeping them in a dynamic array.
 	*/
	dataFile >> inputnumber;
	int* arr = new int [inputnumber];
	
	for (int i = 0; i < inputnumber; i++)
		dataFile >> arr[i];
	/**
	After taking numbers from .txt it sends them to functions.
	And it equalize them to their variables for keeping results.
	*/
	summation =sum(arr, inputnumber);
	product = pro(arr, inputnumber);
	average = avg(arr, inputnumber);
	smallestr = smallest(arr, inputnumber);
	///It is printing results to screen for user.
	cout <<"Sum: " << summation<< endl;
	cout <<"Product: " << product<< endl;
	cout << "Average: " << average << endl;
	cout << "Smallest: " << smallestr << endl;

	dataFile.close();///Closing file.
	delete [] arr;///Deleting dynamic array for heap memory.
	arr = NULL;
	
	system("pause");
}

int sum(int *arr, int n) {
	/**@param result is for returning result of calculation. */
	/**It adds every number to result so it is keeping result of function.
	*It calculates summation of numbers by a "for" loop.
	*/
	int result=0;
	for (int i = 0; i < n; i++)
		result += arr[i];
	return result;
}
int pro(int *arr, int n) {
	/**@param result keeps result of calculation.*/
	/**"result" is equal to 1 at first step. 
	*Because the function product every number with it.
	*Therefore it is keeping result of function.
	*It calculate production of numbers by a "for" loop.
	*/
	int result = 1;
	for (int i = 0; i < n; i++) {
		result = arr[i] * result;
	}
	return result;
}
int avg(int *arr, int n) {
	/**@param result is for returning result of calculation. */
	/**The funcitons adds every number in the array to result,then it divides result to "n".
	*It calculate average of numbers by a "for" loop.
	*/
	int result = 0;
	for (int i = 0; i < n; i++)
		result += arr[i];
	result /= n;
	return result;
}
int smallest(int *arr, int n) {
	/**@param result is for returning result of calculation.*/ 
	/**The funcitons equalize first array number to reuslt at first step. Then it compares result with array number.
	*It calculate average of numbers by a "for" loop.
	*/
	int result = 0;
	for (int i = 0; i < n; i++)
		if (i == 0)
			result = arr[i];
		else
			if (arr[i] < result)
				result = arr[i];
	return result;
}